package com.captain.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.captain.app2.databinding.FragmentMinusBinding
import com.captain.app2.databinding.FragmentPlusBinding


class MinusFragment : Fragment() {
    private var _binding: FragmentMinusBinding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMinusBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.equalButton.setOnClickListener {
            var num1 = binding.firstNumber.text.toString().toInt()
            var num2 = binding.SecNumber.text.toString().toInt()
            var result = num1-num2
            val action = MinusFragmentDirections.actionMinusFragmentToAnswerFragment(result = result.toString())
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

        companion object {

    }
}
