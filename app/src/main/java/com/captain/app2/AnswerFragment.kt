package com.captain.app2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.captain.app2.databinding.FragmentAnswerBinding

class AnswerFragment : Fragment() {
    private var _binding: FragmentAnswerBinding? = null
    private val binding get() = _binding!!
    private lateinit var result: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        result = arguments?.getString(RESULT).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAnswerBinding.inflate(inflater, container, false)
        binding.resultText.text = result
        val view = binding.root

        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.restartButton.setOnClickListener {
            val action = AnswerFragmentDirections.actionAnswerFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

    }

    companion object {
        const val RESULT = "result"
    }
}
